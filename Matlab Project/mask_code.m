% Find the param ID of the combobox so we can set its contents later
maskObj = Simulink.Mask.get(gcb);
par_name = 'ParameterNames';
par_idx = find(strcmp({maskObj.Parameters.Name}, par_name));

% Each ICD is read from the respective ↓ hardcoded ICD filenames here
[num,txt,raw] = xlsread('IcdFiles/CAN_ICD_1.5.csv');
can_opts = raw(2:end,15);

% The A429 ICD Inputs on the STB seem to be sent
% with the label name and param name concatenated 
% with an underscore between them. So for this one
% we read two columns and strcat each corresponding cell
[num,txt,raw] = xlsread('IcdFiles/A429_ICD_Inputs_1.5.csv');
A429_label_names = raw(2:end,5);
A429_param_names = raw(2:end,7);
A429_msg_opts = strcat(A429_label_names, {'_'}, A429_param_names);

[num,txt,raw] = xlsread('IcdFiles/A429_ICD_Outputs_1.5.csv');
A429_out_opts = raw(2:end,7);

[num,txt,raw] = xlsread('IcdFiles/Discrete_Inputs_1.5.csv');
discrete_in = raw(2:end,10);

[num,txt,raw] = xlsread('IcdFiles/RS422_ICD_Outputs_1.5.csv');
rs422_out = raw(2:end, 5);

% Glue all columns together into a single large column
options = [can_opts; A429_msg_opts; A429_out_opts; discrete_in; rs422_out];

% Remove empty cells, 'Spare', 'spare', 'tbd' - feel free to add
% more if needed
options(any(cellfun(@(x) any(isnan(x)),options),2),:) = [];
options = setdiff(options, {'Spare'});
options = setdiff(options, {'spare'});
options = setdiff(options, {'tbd'});

% Set combobox options to our large column of values
maskObj.Parameters(par_idx).TypeOptions = options;