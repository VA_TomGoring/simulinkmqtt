# Simulink MQTT Emulator Control

Defines 3 C++ MEX functions and two C++ S-Function blocks:

| Function | Type | Params | Returns | Description |
| ---      | ---  | ---    | --- | --- |
| init()   | MEX  | Broker IP | Test ID | Requires a config.json file in the main workspace. The format can be found in the same file in this repo.
| run() | MEX | Broker IP & Test ID | None | Sets configured emulators to run. Can be called either before or after sending parameter commands to emulators |
| publish() | S-Function Block | Broker IP, Topic, Parameter/Label name from ICD | Output signal is the MQTT return code from publishing the message, i.e. if the message published successfully or not. | Sends a command on the specified topic (e.g. 'STB_Command/LMC_1 for sending commands to the LMC1 subsystem). Currently schedules all commands to be executed on emulators at time 0, so they are executed immediately. Reads parameter/label names from ICD files - which columns are read are currently hard coded (e.g. column 7 etc). |
| stop() | MEX | Broker IP & Test ID | None | Sets emulators to stopped and then completed. This will trigger the emulators to upload their recorded results to the STB database |
| receive() | S-Function Block | Broker IP & Topic | A single MQTT message value per sample time | Will output MQTT messages received on the current topic when sampled. Will currently block until a message is received. Does not currently expect anything but a single stringified float value as a message - message contents TBD.

## Usage
To use the MEX functions, call them as per normal MATLAB functions while the .mexw64 files are in the current workspace. The 'emulator_mqtt' model file is a library containing the publish & receive blocks, and the callback buttons with MEX functions attached. This library can be used as per the MATLAB documentation.

## Notes
- The simulink library provides both three callback buttons which trigger their respective MEX functions, and publish and receive blocks.
- Doesn't currently verify that the emulators are in the expected state, try to synchronise getting them into states, or handle them not being in the expected state.
- On a local test the emulator just sort of sits after it stops - awaiting restart functionality now
- Will need to write up a config file for the hardware this will be interacting with - mapping physical IDs to subsystem/buses

## Building and Running
The powershell script (build.ps1) should build the binaries. Conan and CMake will need to be installed for this to work. I haven't tested what happens to builds on Linux/WSL since adding the MEX functions, so that might not work currently. The non mex compilation will produce a binary: ./build/bin/simu.exe, which is made from the main.cpp src file. The function headers to control emulators are in include/emulator_mqtt.h.

If you open the Matlab project, you can use startup(), init(), run(), and stop() from the Matlab command line. There is a sample publish block in sfunction_tests.slx to send parameter data with. Drag and drop the MEX functions into the Matlab project folder to update the ones Matlab uses - they are built into the ./build/Release/ directory.

## TODO
- Create Matlab project containing library and MEX functions for easy distribution - project could be built via pipeline - have template project, build MEX functions, add to project?
- [11/10: 15:22] Ben Lawrence: you can [have custom libraries in simulink] im sure - we'd have to have a shared folders/repo and path  - lets put it on the list and we'll figure out a way (overlaps with other issues)
- Add premade model block as starter for work using library blocks.

## Issues
- Library link causes issues when setting the parameter of the s-function blocks.