from conans import ConanFile, CMake

class StbConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"    
    requires = (
        "boost/1.77.0", 
        "paho-mqtt-cpp/1.2.0",
    )
    generators = "cmake_paths", "cmake_find_package", "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()