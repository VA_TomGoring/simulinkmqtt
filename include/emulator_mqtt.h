#include <string>

#pragma warning(push)
#pragma warning(disable: 4834)
#include <mqtt/client.h>
#pragma warning(pop)

void init(std::string broker_ip);
int publish(std::shared_ptr<mqtt::client> cli, std::string topic, std::string message);
int receive(mqtt::client *cli);
int run(std::string broker_ip);
int stop(std::string broker_ip);
void startup(std::string broker_ip, std::string software_version, std::string logging_level);