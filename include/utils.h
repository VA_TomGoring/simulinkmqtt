#include <string>

#pragma warning(push)
#pragma warning(disable: 4834)
#include <mqtt/client.h>
#pragma warning(pop)

std::string get_current_utc_time_as_fmt_string();
std::vector<std::tuple<std::string, std::string>> get_emulator_config(std::string filepath);
