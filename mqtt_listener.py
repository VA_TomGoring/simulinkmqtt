""" MQTT Listener 

This script subscribes to all topics on a localhost MQTT broker and prints all 
received messages and the time of their arrival.
"""

#!/usr/bin/python3

import paho.mqtt.client as mqtt
from datetime import datetime
import sys

def on_connect(client, userdata, flags, rc):
  print("Connected")
  client.subscribe("#")

def on_message(client, userdata, msg):
  print(f"Received msg: {msg.payload} at time: {datetime.now()} on topic: {msg.topic}")

def main(argv):
  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.connect(argv[1], 1883, 60)

  client.loop_forever()


if __name__ == '__main__':
  main(sys.argv)
