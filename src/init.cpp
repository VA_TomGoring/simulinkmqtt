#define S_FUNCTION_NAME init /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

#include "utils.h"

#include <string>
#include <boost/format.hpp>
#include <boost/json.hpp>
#include <chrono>
#include <thread>

const std::string CLIENT_ID{"Simulink Emulator Init Client"};
const std::string TOPIC{"EMU_State_Request"};

using std::string;

void init(std::string broker_ip)
{
    std::cout << "Initialising nodes" << std::endl;
    mqtt::client cli(broker_ip, CLIENT_ID);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(120);
    connOpts.set_clean_session(true);
    connOpts.set_connect_timeout(5);

    std::cout << "Connecting to broker..." << std::endl;
    cli.connect(connOpts);
    std::cout << "Connected to broker" << std::endl;

    boost::json::value jv = {
        {"command", "start"}
    };

    mqtt::message_ptr msg = mqtt::make_message("emulator_control", boost::json::serialize(jv));
    msg->set_qos(1);
    cli.publish(msg);

    std::this_thread::sleep_for(std::chrono::seconds(5));

    std::vector<std::tuple<std::string, std::string>> emulatorConfig = get_emulator_config("./config.json");
    string time = get_current_utc_time_as_fmt_string();

    try
    {
        std::cout << "Sending emulator config..." << std::endl;

        for (auto emulator : emulatorConfig) {
            std::string id = std::get<0>(emulator);
            std::string config = std::get<1>(emulator);

            std::string node_topic = str(boost::format("Node_Config/%1%") % id);

            mqtt::message_ptr pubmsg = mqtt::make_message(node_topic, config);
            pubmsg->set_qos(1);
            cli.publish(pubmsg);
        }

        std::cout << "Node set to ready" << std::endl; // TODO: check if it actually is ready?
    }
    catch (const mqtt::exception &exc)
    {
        std::cerr << "Error: " << exc.what() << " [" << exc.get_reason_code() << "]" << std::endl;
        return;
    }

    cli.disconnect();
}

#ifdef MATLAB_MEX_FILE

#include "mex.hpp"
#include "mexAdapter.hpp"

using namespace matlab::data;
using matlab::mex::ArgumentList;

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(ArgumentList outputs, ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        if (inputs.size() != 0) {
            matlabPtr->feval(u"error", 0, 
            std::vector<matlab::data::Array>({ factory.createScalar("Zero inputs required - init()") }));
        }

        matlab::data::CharArray broker_ip = matlabPtr->getVariable("broker_ip");

        init(broker_ip.toAscii());
    }
};

#endif