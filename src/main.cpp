#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <boost/json.hpp>

#include "emulator_mqtt.h"
#include "utils.h"

const std::string BROKER_IP{"tcp://172.23.237.9:1883"};
const std::string CLIENT_ID{"Simulink Client"};

using std::cout;
using std::endl;

std::string build_json_tmp(std::string parameter, double value) {
    boost::json::value setParameterMessageRaw = {
        {"command",
         {{"type", "set_parameter"},
          {"data", {{"parameter", parameter}, {"value", value}}}}},
        {"time", 0.0}};
    std::string message = boost::json::serialize(setParameterMessageRaw);
    return message;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        exit(1);
    } else {
        std::string ip(argv[1]);
        std::string command(argv[2]);
        if (command == "init") {
            init(ip);
        } else if (command == "run") {
            run(ip);
        } else if (command == "stop") {
            stop(ip);
        } else if (command == "publish") {
            auto cli = std::make_shared<mqtt::client>(ip, CLIENT_ID);
            mqtt::connect_options connOpts;
            connOpts.set_keep_alive_interval(120);
            connOpts.set_clean_session(true);
            cli->connect(connOpts);
            publish(cli, "STB_Command/AHRS_1", build_json_tmp("IR Discrete Word #1_Heading Invalid", 1.0));
        } else {
            std::cout << "Not a valid CLI argument" << std::endl;
        }
    }
}