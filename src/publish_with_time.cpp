#define S_FUNCTION_NAME publish_w_time /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

#ifdef MATLAB_MEX_FILE
#include "simstruc.h"
#endif

#include "utils.h"

#include <boost/json.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using boost::format;

const std::string CLIENT_ID{"Simulink Publish with Time Client/"};
const std::string COMMAND_TOPIC{"STB_Command/"};

std::map<std::string, std::shared_ptr<mqtt::client>> client_map;

std::string build_json(std::string parameter, double value, time_t time) {
    boost::json::value setParameterMessageRaw = {
        {"command", {
             {"type", "set_parameter"},
             {"data", {
                  {"parameter", parameter},
                  {"value", value}
             }}
        }},
        {"time", time}
    };
    std::string message = boost::json::serialize(setParameterMessageRaw);
    return message;
}

int publish(std::shared_ptr<mqtt::client> cli, std::string topic, std::string message) {
    try {
        mqtt::message_ptr pubmsg = mqtt::make_message(topic, message);
        pubmsg->set_qos(1);
        cli->publish(pubmsg);
    }
    catch (const mqtt::exception &exc) {
        std::cout << format("MQTT message failed to send with reason: %s and status code: %d") %
            exc.what() % exc.get_reason_code() << std::endl;

        return exc.get_reason_code();
    }

    return 0;
}

#ifdef MATLAB_MEX_FILE

std::string get_topic(SimStruct *S) {
    std::string topic(mxArrayToString(ssGetSFcnParam(S, 0)));
    return topic;
}

std::string get_parameter(SimStruct *S) {
    std::string parameter(mxArrayToString(ssGetSFcnParam(S, 1)));
    return parameter;
}

std::string get_broker_ip(SimStruct *S) {
    std::string broker_ip(mxArrayToString(ssGetSFcnParam(S, 2)));
    return broker_ip;
}

static void mdlInitializeSizes(SimStruct *S) {
    // Params: BrokerIP, Topic, Parameter
    ssSetNumSFcnParams(S, 3);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch reported by the Simulink engine*/
    }
    
    // Inputs: Message value
    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    // Outputs: MQTT publish return code
    if (!ssSetNumOutputPorts(S,1)) return;
    ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_ENABLE
static void mdlEnable(SimStruct *S) {
    std::string broker_ip = get_broker_ip(S);
    std::cout << "Broker IP: '" << broker_ip << "'" << std::endl;
    std::string parameter = get_parameter(S);
    std::cout << "Checking if MQTT client for current parameter: " << parameter << " exists..." << std::endl;
    auto it = client_map.find(parameter);

    if (it == client_map.end()) {
        std::cout << "Inserting new mqtt connection for parameter: " << parameter << std::endl;
        client_map[parameter] = std::make_shared<mqtt::client>(get_broker_ip(S), CLIENT_ID + parameter);
        client_map[parameter]->connect();
    }
}

static void mdlOutputs(SimStruct *S, int_T tid) {
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0); // uPtrs is an array of pointers to output signals of type double
    real_T *y = ssGetOutputPortRealSignal(S,0); // pointer to output signals of type double

    std::string topic = COMMAND_TOPIC + get_topic(S);
    std::string message = build_json(get_parameter(S), *uPtrs[0], ssGetT(S));
    std::string parameter = get_parameter(S);

    if (client_map.find(parameter) != client_map.end()) {
        std::cout << "Found client for parameter: " << parameter << std::endl;
        if (!client_map[parameter]->is_connected()) {
            std::cout << "Client is not connected..." << std::endl;
            client_map[parameter]->connect();
        }
        *y = publish(client_map[parameter], topic, message);
    } else {
        std::cout << "Could not find client for parameter: " << parameter << std::endl;
        client_map[parameter] = std::make_shared<mqtt::client>(get_broker_ip(S), CLIENT_ID + parameter);
    }
}

static void mdlTerminate(SimStruct *S) {
    std::string parameter = get_parameter(S);
    if (client_map.find(parameter) != client_map.end()) {
        if (client_map[parameter]->is_connected()) {
            client_map[parameter]->disconnect();
        }
    }
}

#endif

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c" /* MEX-file interface mechanism */
#endif