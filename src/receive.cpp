
#define S_FUNCTION_NAME receive /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

#ifdef MATLAB_MEX_FILE
#include "simstruc.h"
#endif

#include <iostream>

#include "utils.h"

std::unique_ptr<mqtt::client> rx_client; // Global MQTT client maintained between outputs so we don't make a new connection per message
const std::string CLIENT_ID{"Simulink Receive Client"};

int receive(mqtt::client *cli) {
    try {
        auto msg = cli->consume_message();
        std::cout << msg->to_string() << std::endl;
        return 0;
    }
    catch (const mqtt::exception &ex) {
        std::cout << ex.what() << std::endl;
        return ex.get_reason_code();
    }
}

#ifdef MATLAB_MEX_FILE

std::string get_broker_ip(SimStruct *S) {
    std::string broker_ip(mxArrayToString(ssGetSFcnParam(S, 0)));
    return broker_ip;
}

std::string get_topic(SimStruct *S) {
    std::string topic(mxArrayToString(ssGetSFcnParam(S, 1)));
    std::cout << "Topic: " << topic << std::endl;
    return topic;
}

static void mdlInitializeSizes(SimStruct *S) {
    // ? - TBD: do we need to listen for a specific parameter?
    // Params: BrokerIP, Topic
    ssSetNumSFcnParams(S, 2);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch reported by the Simulink engine*/
    }
    
    if (!ssSetNumInputPorts(S, 0)) return;

    // Outputs: Stub 0 - JSON should be processed and value returned
    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_ENABLE
static void mdlEnable(SimStruct *S) {
    std::string broker_ip = get_broker_ip(S);
    std::string topic = get_topic(S);
    rx_client = std::make_unique<mqtt::client>(broker_ip, CLIENT_ID);
    if (!rx_client->is_connected()) {
        rx_client->connect();
        std::cout << "Connected to MQTT broker";
        rx_client->subscribe(topic);
    }
}

static void mdlOutputs(SimStruct *S, int_T tid) {
    real_T *out = ssGetOutputPortRealSignal(S, 0); // pointer to output signals of type double
    auto msg = rx_client->consume_message();
    std::cout << msg->to_string() << std::endl;
    float y = std::stof(msg->to_string());
    *out = y; // TODO: process incoming message and output payload value
}

static void mdlTerminate(SimStruct *S) {
    if (rx_client->is_connected()) {
        rx_client->disconnect();
    }
}

#endif

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c" /* MEX-file interface mechanism */
#endif