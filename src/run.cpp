#define S_FUNCTION_NAME run /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

// #ifdef MATLAB_MEX_FILE
// #include "simstruc.h"
// #endif

#include "utils.h"

#include <iostream>
#include <boost/json.hpp>
#include <string>
#include <chrono>
#include <thread>
#include <fstream>
#include <sstream>

const std::string CLIENT_ID{"Simulink Emulator Run Client"};
const std::string COMMAND_TOPIC{"STB_Command/"};

int run(std::string broker_ip)
{
    std::cout << "Switching emulators to running" << std::endl;
    mqtt::client cli(broker_ip, CLIENT_ID);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(120);
    connOpts.set_clean_session(true);
    connOpts.set_connect_timeout(5);

    cli.connect(connOpts);

    boost::json::value enableCommand = {
                        {"command", {
                                                {"type", "enable_subsystem"},
                                                {"state", true},
                                             }},
                                  {"time", 0.0}
                                };

    std::ifstream t("./config.json");
    std::stringstream buffer;
    buffer << t.rdbuf();
    std::string config = buffer.str();

    auto jv = boost::json::parse(config);
    auto obj = jv.as_object();
    auto emulators = obj["emulators"].as_array();

    for (auto emulator : emulators) {
        auto subsystems = emulator.as_object()["config"].as_object()["subsystems"].as_array();
        for (auto subsystem : subsystems) {
            auto topic = COMMAND_TOPIC + (std::string)subsystem.as_object()["name"].as_string();
            mqtt::message_ptr msg = mqtt::make_message(topic, boost::json::serialize(enableCommand));
            msg->set_qos(1);
            cli.publish(msg);
        }
    }

    std::string current_time = get_current_utc_time_as_fmt_string();

    boost::json::value changeState = {
        {"state", "Running"},
        {"test_id", -1},
        {"start_time", current_time}};

    std::string run_message = boost::json::serialize(changeState);

    mqtt::message_ptr pubmsg = mqtt::make_message("EMU_State_Request", boost::json::serialize(changeState));
    pubmsg->set_qos(1);
    cli.publish(pubmsg);

    std::cout << "Node set to run" << std::endl;
    cli.disconnect();

    return 0;
}

#ifdef MATLAB_MEX_FILE

#include "mex.hpp"
#include "mexAdapter.hpp"

using namespace matlab::data;
using matlab::mex::ArgumentList;

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(ArgumentList outputs, ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        if (inputs.size() != 0)
        {
            matlabPtr->feval(u"error", 0,
                             std::vector<matlab::data::Array>({factory.createScalar("Zero inputs required - run()")}));
        }

        if (outputs.size() != 0)
        {
            matlabPtr->feval(u"error", 0,
                             std::vector<matlab::data::Array>({factory.createScalar("Zero outputs required")}));
        }

        matlab::data::CharArray broker_ip = matlabPtr->getVariable("broker_ip");
        run(broker_ip.toAscii());
    }
};

#endif