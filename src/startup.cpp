
#define S_FUNCTION_NAME startup /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

#include "utils.h"

#include <string>
#include <boost/format.hpp>
#include <boost/json.hpp>
#include <chrono>
#include <thread>

const std::string CLIENT_ID{"Simulink Emulator Init Client"};
const std::string TOPIC{"EMU_State_Request"};

using std::string;

void startup(string broker_ip, string software_version, string logging_level)
{
    mqtt::client cli(broker_ip, CLIENT_ID);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(120);
    connOpts.set_clean_session(true);

    std::cout << "Connecting to broker..." << std::endl;
    cli.connect(connOpts);
    cli.subscribe("emulator_control");
    std::cout << "Connected to broker" << std::endl;

    boost::json::value jv = {
        {"software_version", {{"VA_SW_002", software_version}, {"VA_SW_006", "latest"}}},
        {"device_log_level", logging_level}};
    std::string ansibleSetupJson = boost::json::serialize(jv);

    mqtt::message_ptr ansibleSetupMsg = mqtt::make_message("emulator_control", ansibleSetupJson);
    ansibleSetupMsg->set_qos(1);
    cli.publish(ansibleSetupMsg);
    std::cout << "Sent message to setup emulators via ansible" << std::endl;

    std::cout << "Waiting for emulator to be ready..." << std::endl;
    // TODO: don't wait unless we have confirmation that the ansible script is running

    bool done = false;
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    while (!done)
    {
        if (std::chrono::steady_clock::now() - start > std::chrono::seconds(60))
            break;

        mqtt::const_message_ptr ptr;
        if (cli.try_consume_message_for(&ptr, std::chrono::seconds(5))) {
            auto msg = boost::json::parse(ptr->to_string());
            if (msg.is_object())
            {
                auto obj = msg.as_object();
                if (obj.contains("status"))
                {
                    if (obj["status"] == "successful")
                    {
                        std::cout << "Emulator waiting for config..." << std::endl;
                        break;
                    }
                }
            }
        }

        std::cout << "Still waiting..." << std::endl;
    }
}

#ifdef MATLAB_MEX_FILE

#include "mex.hpp"
#include "mexAdapter.hpp"

using namespace matlab::data;
using matlab::mex::ArgumentList;

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(ArgumentList outputs, ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        if (inputs.size() != 2)
        {
            matlabPtr->feval(u"error", 0,
                             std::vector<matlab::data::Array>({factory.createScalar("Two inputs required - init(software_version, logging_level)")}));
        }

        matlab::data::CharArray broker_ip = matlabPtr->getVariable("broker_ip");
        matlab::data::CharArray software_version = inputs[0];
        matlab::data::CharArray logging_level = inputs[1];

        startup(broker_ip.toAscii(), software_version.toAscii(), logging_level.toAscii());
    }
};

#endif