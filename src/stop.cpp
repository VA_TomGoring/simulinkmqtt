#define S_FUNCTION_NAME stop /* Defines and Includes */
#define S_FUNCTION_LEVEL 2

// #ifdef MATLAB_MEX_FILE
// #include "simstruc.h"
// #endif

#include "utils.h"

#include <iostream>
#include <boost/json.hpp>
#include <chrono>
#include <thread>

const std::string TOPIC{"EMU_State_Request"};
const std::string CLIENT_ID{"Simulink Emulator Stop Client"};

int stop(std::string broker_ip) {
    mqtt::client cli(broker_ip, CLIENT_ID);
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(120);
    connOpts.set_clean_session(true);
    connOpts.set_connect_timeout(5);

    cli.connect(connOpts);

    std::string current_time = get_current_utc_time_as_fmt_string();

    boost::json::value changeState = {
        {"state", "Stopping"},
        {"test_id", -1},
        {"start_time", current_time}};

    std::string run_message = boost::json::serialize(changeState);

    // TODO: try catch around mqtt for error handling
    mqtt::message_ptr pubmsg = mqtt::make_message("EMU_State_Request", boost::json::serialize(changeState));
    pubmsg->set_qos(1);
    cli.publish(pubmsg);

    std::cout << "Node set to stopping" << std::endl;

    changeState = {
        {"state", "Completed"},
        {"test_id", -1},
        {"start_time", current_time}};

    pubmsg = mqtt::make_message("EMU_State_Request", boost::json::serialize(changeState));
    pubmsg->set_qos(1);
    cli.publish(pubmsg);

    std::cout << "Node set to completed" << std::endl;
    
    boost::json::value jv = {
        {"command", "stop"}
    };

    mqtt::message_ptr msg = mqtt::make_message("emulator_control", boost::json::serialize(jv));
    msg->set_qos(1);
    cli.publish(msg);

    cli.disconnect();
    return 0;
}

#ifdef MATLAB_MEX_FILE

#include "mex.hpp"
#include "mexAdapter.hpp"

using namespace matlab::data;
using matlab::mex::ArgumentList;

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(ArgumentList outputs, ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        if (inputs.size() != 0) {
            matlabPtr->feval(u"error", 0, 
            std::vector<matlab::data::Array>({ factory.createScalar("One input required - stop(broker_ip)") }));
        }

        if (outputs.size() != 0) {
            matlabPtr->feval(u"error", 0, 
            std::vector<matlab::data::Array>({ factory.createScalar("Zero outputs required") }));
        }

        matlab::data::CharArray broker_ip = matlabPtr->getVariable("broker_ip");
        stop(broker_ip.toAscii());
    }
};

#endif
