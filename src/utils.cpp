#include <iostream>
#include <boost/json.hpp>
#pragma warning(push)
#pragma warning(disable: 4834)
#include <mqtt/client.h>
#pragma warning(pop)
#include <chrono>
#include <string>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <tuple>

std::string get_current_utc_time_as_fmt_string()
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    in_time_t += 5;
    std::stringstream ss;
    ss << std::put_time(std::gmtime(&in_time_t), "%FT%TZ");
    return ss.str();
}

std::vector<std::tuple<std::string, std::string>> get_emulator_config(std::string filepath) {
    try {
        std::ifstream t(filepath);

        std::stringstream buffer;
        buffer << t.rdbuf();
        std::string config = buffer.str();

        std::vector<std::tuple<std::string, std::string>> emulatorConfig;

        try {
            auto jv = boost::json::parse(config);
            auto obj = jv.as_object();
            auto emulators = obj["emulators"].as_array();

            for (auto emulator : emulators) {
                std::string id = boost::json::serialize(emulator.as_object()["id"]);
                id.erase(remove(id.begin(), id.end(), '\"'), id.end()); // Remove extra quotes from string,
                                                                        // not sure why they are there.

                std::string config = boost::json::serialize(emulator.as_object()["config"]);
                std::tuple<std::string, std::string> pair = std::make_tuple(id, config);
                emulatorConfig.push_back(pair);
            }
        } 
        catch (const std::exception& e) {
            std::cout << "Caught exception while parsing JSON config: " << e.what() << std::endl;
        }

        return emulatorConfig;
    } 
    catch (const std::filesystem::filesystem_error& e) {
        std::cout << "Caught exception while reading config file: " << e.what() << std::endl;
    }
}